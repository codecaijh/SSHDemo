/*
 * @(#) LoginController
 * <br> Copyright:  Copyright (c) 2017
 * <br> @author 蔡金煌
 * <br> 2017-10-29 15:49:11
 */

package controll;


import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;


import javax.annotation.Resource;

import service.serviceInterface.UserService;

@Controller
public class LoginController{

    @Resource
    private UserService userService;

    @RequestMapping("/index.do")
    public String index(){
        // userService.updateUserInfo();
        userService.insertUserInfo(null);
        return "index";
    }

}

