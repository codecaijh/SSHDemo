/*
 * @(#) UserDao
 * <br> Copyright:  Copyright (c) 2017
 * <br> @author 蔡金煌
 * <br> 2017-10-29 15:49:43
 */

package dao.daoInterface;

import java.util.List;

import domain.UserEntity;

public interface UserDao {
    public List<UserEntity> getUsersInfo();
    public int insertUserInfo(UserEntity userEntity);
    public int deleteUserInfo();
    public int updateUserInfo();
}
