/*
 * @(#) UserDaoImpl
 * <br> Copyright:  Copyright (c) 2017
 * <br> @author 蔡金煌
 * <br> 2017-10-29 15:49:32
 */

package dao.daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;

import constants.HqlConstant;
import dao.daoInterface.UserDao;
import domain.UserEntity;

@Repository("userDao")
public class UserDaoImpl implements UserDao{
    @Resource
    SessionFactory sessionFactory;

    public List<UserEntity> getUsersInfo() {
        return sessionFactory.getCurrentSession().createQuery(HqlConstant.GET_ALLUSERINFO).list();
    }

    public int deleteUserInfo() {
        return 0;
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    public int insertUserInfo(UserEntity userEntity) {
        Session session = sessionFactory.getCurrentSession();
        session.save(userEntity);
        session.flush();
        session.clear();
        return 0;
    }

    public int updateUserInfo() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "update UserEntity user set user.username='改1次' where user.id=1";
        session.createQuery(hql).executeUpdate();
        hql = "update UserEntity user set user.username='改2次' where user.id=1";
        session.createQuery(hql).executeUpdate();
        return 0;
    }
}
