/*
 * @(#) UserService
 * <br> Copyright:  Copyright (c) 2017
 * <br> @author 蔡金煌
 * <br> 2017-10-29 15:50:43
 */

package service.serviceInterface;

import java.util.List;

import javax.annotation.Resource;

import dao.daoInterface.UserDao;
import domain.UserEntity;

public interface UserService {
    public List<UserEntity> getUsersInfo();
    public int insertUserInfo(UserEntity userEntity);
    public int deleteUserInfo();
    public int updateUserInfo();
}
