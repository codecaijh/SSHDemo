/*
 * @(#) UserServiceImpl
 * <br> Copyright:  Copyright (c) 2017
 * <br> @author 蔡金煌
 * <br> 2017-10-29 15:50:33
 */

package service.serviceImpl;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

// import dao.daoImpl.SimpleHibernateDao;
import dao.daoInterface.UserDao;
import domain.UserEntity;
import service.serviceInterface.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    // private SimpleHibernateDao<UserEntity, Integer> ldDao;
    //
    // @Autowired
    // public void setSessionFactory(SessionFactory sessionFactory) {
    //     ldDao = new SimpleHibernateDao<UserEntity, Integer>(sessionFactory, UserEntity.class);
    // }

    public List<UserEntity> getUsersInfo() {
        return userDao.getUsersInfo();
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    public int insertUserInfo(UserEntity userEntity) {
        for(int i=0;i<10;i++){
            UserEntity user = new UserEntity();
            user.setUsername("测试"+i);
            user.setPassword("测试"+i);
            user.setAddress("测试"+i);
            user.setCreateTime(new Timestamp(new Date().getTime()));
            user.setUpdateTime(new Timestamp(new Date().getTime()));
            System.out.println("执行："+i+"次");
            userDao.insertUserInfo(user);
        }
        return 0;
    }

    public int deleteUserInfo() {
        return 0;
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.READ_COMMITTED)
    public int updateUserInfo() {
        userDao.updateUserInfo();
        return 0;
    }
}
